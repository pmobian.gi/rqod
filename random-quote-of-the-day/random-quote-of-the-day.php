<?php
/**
 * Plugin Name: Random Quote of the Day (RQOD) by PM
 * Plugin URI: https://www.genious.com/random-quote-of-the-day
 * Description: Displays a random quote of the day on your website.
 * Version: 1.0.0
 * Author: PMN
 * Author URI: https://your-website.com
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: random-quote-of-the-day
 * Domain Path: /languages
 */

// Prevent direct access to this file
if (!defined('ABSPATH')) {
    exit;
}

// Plugin initialization and logic goes here...

// Enqueue stylesheets and scripts
function random_quote_of_the_day_enqueue_scripts() {
    wp_enqueue_style('random-quote-of-the-day-style', plugin_dir_url(__FILE__) . 'assets/css/style.css');
    wp_enqueue_script('random-quote-of-the-day-script', plugin_dir_url(__FILE__) . 'assets/js/script.js', array('jquery'), '1.0.0', true);
}
add_action('wp_enqueue_scripts', 'random_quote_of_the_day_enqueue_scripts');