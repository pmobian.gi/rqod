<?php
class Random_Quote_Widget extends WP_Widget {
    // Widget setup
    function __construct() {
        parent::__construct(
            'random_quote_widget',
            __('Random Quote of the Day', 'random-quote-of-the-day'),
            array('description' => __('Displays a random quote of the day.', 'random-quote-of-the-day'))
        );
    }

    // Display the widget
    function widget($args, $instance) {
        // Widget frontend output
        echo $args['before_widget'];
        echo $args['before_title'] . $instance['title'] . $args['after_title'];

        // Display the quote here
        $quote = $this->get_random_quote();
        echo '<div class="quote">' . $quote . '</div>';

        echo $args['after_widget'];
    }

    // Retrieve a random quote
    function get_random_quote() {
        // Implement your logic to fetch a random quote from the database or an API
        // For simplicity, let's use a static quote for now
        $quotes = array(
            "Quote 1",
            "Quote 2",
            "Quote 3"
        );
        $random_quote = $quotes[array_rand($quotes)];
        return $random_quote;
    }

    // Widget backend settings
    function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Quote of the Day', 'random-quote-of-the-day');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    // Save widget settings
    function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

// Register the widget
function random_quote_of_the_day_register_widget() {
    register_widget('Random_Quote_Widget');
}
add_action('widgets_init', 'random_quote_of_the_day_register_widget');